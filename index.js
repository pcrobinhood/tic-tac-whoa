$('document').ready(function() {

  // ******************
  // Game State
  // ******************

  var BaseGrid = {
    y1x1: {
      value: 2,
      occupied: false
    },
    y1x2: {
      value: 1,
      occupied: false
    },
    y1x3: {
      value: 2,
      occupied: false
    },
    y2x1: {
      value: 1,
      occupied: false
    },
    y2x2: {
      value: 2,
      occupied: false
    },
    y2x3: {
      value: 1,
      occupied: false
    },
    y3x1: {
      value: 2,
      occupied: false
    },
    y3x2: {
      value: 1,
      occupied: false
    },
    y3x3: {
      value: 2,
      occupied: false
    }
  };

  var gameState = {
    grid: $.extend(true, {}, BaseGrid),
    playersTurn: false,
    playerIcon: 'X',
    aiIcon: 'O',
    gameOver: false,
    winningArr: []
  }

  //************
  // Click Functions
  // ***********

  // Player chooses token
  $('.choose').click(function() {
    // Turn players token color
    $(this).css('color', 'darkmagenta');
    var playerIcon = $(this).attr('data');
    var aiIcon = $(this).siblings('.choose').attr('data');
    // Turn opponents token color
    $(this).siblings('.choose').css('color', 'darkkhaki');
    // Assign corresponding tokens
    gameState.playerIcon = playerIcon;
    gameState.aiIcon = aiIcon;
    // clear message area
    setTimeout(function() {
      $('#message-area').html('');
      firstMove();
    }, 500);
  });

  // Players token move
  $('.gridrow li').click(function() {
    if (gameState.playersTurn === true && gameState.gameOver === false) {
      var square = this.id;
      var piece = gameState.playerIcon;
      if (!checkOccupied(square)) {
        placePiece(square, piece, 'player');
        gameState.playersTurn = false;
        var talkOrNot = getRandom(1, 10);
        if (talkOrNot > 5) {
          getMessage('nobestMove');
        }
        aiMove();

      }
    }
  });

  //*************
  // DOM Functinons
  //*************

  // Place an icon on the grid
  function placePiece(square, icon, player) {
    $('#' + square).children('.square').html('<p class="' + player + '">' + icon + '</p>');
    // Notate occupied space
    gameState.grid[square].occupied = player;
    // Check for 'Win!'
    if (checkWin(player, gameState.grid)) {
      getMessage(player + 'Winner');
      rainbowWin(gameState.winningArr);
      gameState.gameOver = true;
      reset();
    }
    valueDiags();
  };

  //Make winner rainbow blasted
  function rainbowWin(arr) {
    arr.forEach(function(square) {
      $('#' + square).children('.square').addClass('rainbowwin');
    });
  }

  function clearBoard() {
    var square;
    for (var key in gameState.grid) {
      square = key;
      $('#' + square).children('.square').html('');
    }
  }

  function reset() {
    // Wait three seconds & then reset
    setTimeout(function() {
      for (var key in gameState.grid) {
        $('#' + key).children('.square').removeClass('rainbowwin');
      }
      gameState.grid = $.extend(true, {}, BaseGrid);
      gameState.playersTurn = false;
      gameState.gameOver = false;
      gameState.winningArr = [];
      clearBoard();
      firstMove();
    }, 3000);
  };

  //***************
  // Game ai
  //***************

  function firstMove() {
    var num = getRandom(1, 2);
    if (num === 2) {
      getMessage('aigoesfirst');
      setTimeout(function() {
        aiMove();
      }, 1000);
    } else {
      getMessage('aigoeslast');
      gameState.playersTurn = true;
    }
  }

  function aiMove() {
    // If game is over reset
    if (gameState.gameOver === true) {
      console.log('Awww a tie?! :/');
      reset();
    };
    // 
    setTimeout(function() {
      var finished = false;
      // ai gets possibles sorted into value
      var possibleMoves = sortGrid(gameState.grid);
      // Check for a tie
      if (possibleMoves.length === 0) {
        getMessage('tieGame');
        gameState.gameOver = true;
        reset();
        return;
      }
      // ai checks for win, makes if so
      for (var i = 0; i < possibleMoves.length; i++) {
        if (detectWinningMove(possibleMoves[i][0], 'ai')) {
          placePiece(possibleMoves[i][0], gameState.aiIcon, 'ai');
          finsihed = true;
          return;
        }
      }
      if (finished) {
        return;
      }
      // If no win, ai check for player block, makes if so
      for (var i = 0; i < possibleMoves.length; i++) {
        if (detectWinningMove(possibleMoves[i][0], 'player')) {
          placePiece(possibleMoves[i][0], gameState.aiIcon, 'ai');
          finsihed = true;
          gameState.playersTurn = true;
          return;
        }

      }
      if (finished) {
        return;
      }
      // If no win/block, ai makes random move based on value
      var bestMove = getbestMove(possibleMoves);
      var max = bestMove.length;
      var num = getRandom(1, max) - 1;
      placePiece(bestMove[num][0], gameState.aiIcon, 'ai');
      // Check for tie
        possibleMoves = sortGrid(gameState.grid);
        if (possibleMoves.length === 0) {
          getMessage('tieGame');
          gameState.gameOver = true;
          reset();
          return;
        }
        gameState.playersTurn = true;
        return;
    }, 500);
  }

  // sort gameState.gridValue, removes occupied squares from result
  function sortGrid(grid) {
    var sortable = [];
    // change grid into array, ignore occupied squares
    for (var key in grid) {
      if (!grid[key].occupied) {
        sortable.push([key, grid[key].value]);
      }
    }
    // sort array by value
    sortable.sort(function(a, b) {
      return (b[1] - a[1]);
    });
    // return array
    return sortable;

  }

  function valueDiags() {
    // Strategy based on open corners. If there is one & center is empty, take center. If three corners are occupied or two & the center, stay away.
    var occupiedDiags = 0;
    var square;
    var row;
    var col;
    for (var key in gameState.grid) {
      square = key;
      row = square[1];
      col = square[3];
      if (gameState.grid[key].occupied) {
        if ((row == 1 && col == 3) || (row == 3 && col == 1) || (row == col)) {
          occupiedDiags++;
        }
      }
    }
    if (occupiedDiags > 0 && checkOccupied('y2x2') === false) {
      gameState.grid.y2x2.value++;

    } else if ((occupiedDiags > 1 && checkOccupied('y2x2')) || occupiedDiags > 3) {
      gameState.grid.y1x1.value = 1;
      gameState.grid.y3x1.value = 1;
      gameState.grid.y1x3.value = 1;
      gameState.grid.y3x3.value = 1;
      gameState.grid.y2x2.value = 1;
    }

  }

  // check if square is occupied
  function checkOccupied(square) {
    if (gameState.grid[square].occupied !== false) {
      return true;
    } else {
      return false;
    }
  }

  // check for win move
  function detectWinningMove(square, player) {
    var grid = $.extend(true, {}, gameState.grid);
    grid[square].occupied = player;
    if (checkWin(player, grid)) {
      return true;
    } else {
      return false;
    }
  }

  function getRandom(min, max) {
    return Math.floor(Math.random() * max) + min;
  }

  function getbestMove(moves) {
    var result = [];
    var score = moves[0][1];
    moves.forEach(function(element) {
      if (element[1] === score) {
        result.push(element);
      }
    });
    return result;
  }

  // check Win!
  function checkWin(player, grid) {
    // check  y1x1 diagonal
    if (grid.y1x1.occupied === player && grid.y2x2.occupied === player && grid.y3x3.occupied === player) {
      gameState.winningArr = ['y1x1', 'y2x2', 'y3x3'];
      return true;
    }
    // check y1x3 diagonal
    if (grid.y1x3.occupied === player && grid.y2x2.occupied === player && grid.y3x1.occupied === player) {
      gameState.winningArr = ['y1x3', 'y2x2', 'y3x1'];
      return true;
    }
    // check verticals
    if (grid.y1x1.occupied === player && grid.y2x1.occupied === player && grid.y3x1.occupied === player) {
      gameState.winningArr = ['y1x1', 'y2x1', 'y3x1'];
      return true;
    }
    if (grid.y1x2.occupied === player && grid.y2x2.occupied === player && grid.y3x2.occupied === player) {
      gameState.winningArr = ['y1x2', 'y2x2', 'y3x2'];
      return true;
    }
    if (grid.y1x3.occupied === player && grid.y2x3.occupied === player && grid.y3x3.occupied === player) {
      gameState.winningArr = ['y1x3', 'y2x3', 'y3x3'];
      return true;
    }
    // check horizontals
    if (grid.y1x1.occupied === player && grid.y1x2.occupied === player && grid.y1x3.occupied === player) {
      gameState.winningArr = ['y1x1', 'y1x2', 'y1x3'];
      return true;
    }
    if (grid.y2x1.occupied === player && grid.y2x2.occupied === player && grid.y2x3.occupied === player) {
      gameState.winningArr = ['y2x1', 'y2x2', 'y2x3'];
      return true;
    }
    if (grid.y3x1.occupied === player && grid.y3x2.occupied === player && grid.y3x3.occupied === player) {
      gameState.winningArr = ['y3x1', 'y3x2', 'y3x3'];
      return true;
    }
    return false;
  }

  //*************
  // Messages
  //*************
  function getMessage(condition) {
    var max = messages[condition].length;
    var num = getRandom(1, max) - 1;
    var msg = messages[condition][num];
    $('#message-area').html('<h2>' + msg + '</h2>');
  }

  var messages = {
    aigoesfirst: ["Step back, let me take a shot","You are so gonna lose","Guess I'll go first","Don't quit yet, its my turn"],
    aigoeslast: ["I'm waiting.....","Good luck, you gonna need it","I'm feeling very generous today, you may go first","Whenever you're ready bud..."],
    nobestMove: ["I'm really doing my best here....","Thats what I'm talking about","We've got a real thinker going on here","interesting...","touche","Meow ^-^"],
    playerWinner: ["Wow, you won at tic tac toe","You're really good at this!","Now Pinky, it's time to take over the world!","You must have been paying attention this time"],
    aiWins: ["So close","I could get used to this","Thats what happens when you play with a master","Are you actually trying??","I feel AMAZINGGG!!!"],
    tieGame: ["We both won!!!! ;)", "CAT","Let's pretend this never happened","It's like you know what I'm going to do next","This is absolutely riveting","I feel like we are in the movie Groundhog Day"]
  };

}); // end document.ready
